#include <linux/version.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/highmem.h>
#include <asm/unistd.h>

MODULE_LICENSE("GPL");

unsigned long *sys_call_table = (unsigned long*)0xffffffff98800160;

asmlinkage int (*real_unlinkat)(const char *pathname);

asmlinkage int custom_unlinkat(const char *pathname)
{
	printk("Se intercepto: unlinkat(%s)\n", pathname);
	return -1;
	//return real_unlinkat(pathname);
}

int make_rw(unsigned long address)
{
	unsigned int level;
	pte_t *pte = lookup_address(address, &level);
	if(pte->pte &~ _PAGE_RW){
		pte->pte |= _PAGE_RW;
		printk("RW seteado\n");
	}
	return 0;
}

int make_ro(unsigned long address)
{
	unsigned int level;
	pte_t *pte = lookup_address(address, &level);
	pte->pte = pte->pte &~ _PAGE_RW;
	printk("RO seteado\n");
	return 0;
}


static int __init init_papelera(void)
{
	printk(KERN_INFO "Iniciando instalacion del modulo\n");

	make_rw((unsigned long)sys_call_table);
	real_unlinkat = (void *)sys_call_table[__NR_unlinkat];
	*(sys_call_table + __NR_unlinkat) = (unsigned long)custom_unlinkat;

	printk("Llamada al sistema interceptada");

	return 0;
}

static void __exit exit_papelera(void)
{
	
	make_rw((unsigned long)sys_call_table);
	*(sys_call_table + __NR_unlinkat) = (unsigned long)real_unlinkat;
	make_ro((unsigned long)sys_call_table);
	printk(KERN_INFO "Modulo desinstalado\n");
}

module_init(init_papelera);
module_exit(exit_papelera);